# README #

This API can be used to simplify the process of using the Google APIs

### What is this repository for? ###

* Geocode
* Timezone
* Routes on maps

![Untitled.png](https://bitbucket.org/repo/RAqqGk/images/3395732870-Untitled.png)

### How do I get set up? ###

You can install the package from Nuget:

* Add http://nuget.qa.engagedweb.net/nuget to your repository feed.
* PM> Install-Package Google.Api.Helper
* Look the sample project for examples

### Get Time Zone ###
```
#!c#

IFormatProvider culture = new CultureInfo("en-GB", true);
GoogleTimeZone timezoneDep = NetGoogleGeocoding.GetTimeZone(locationFrom.Text, DateTime.ParseExact(fromDate.Text, "dd/MM/yyyy HH:mm:ss", culture));
GoogleTimeZone timezoneArr = NetGoogleGeocoding.GetTimeZone(locationTo.Text, DateTime.ParseExact(toDate.Text, "dd/MM/yyyy HH:mm:ss", culture));

```

### Get address info ###

```
#!c#

var geocode1 = NetGoogleGeocoding.GoogleGeocode("London, UK");
var lat1 = geocode1.GeoCodes[0].Latitude;
var long1 = geocode1.GeoCodes[0].Longitude;
                
var geocode2 = NetGoogleGeocoding.GoogleGeocode(locationTo.Text);
var lat2 = geocode2.GeoCodes[0].Latitude;
var lat2 = geocode2.GeoCodes[0].Longitude;
```

### Calculate time-zone difference ###


```
#!c#

 TimeSpan span =
                                 ((DateTime.ParseExact("18/07/2014 13:17:07", "dd/MM/yyyy HH:mm:ss", culture).AddHours((timezoneArr.DayLightSavingOffset + timezoneArr.Offset) * -1)) -
                                  DateTime.ParseExact("18/07/2014 13:17:00", "dd/MM/yyyy HH:mm:ss", culture).AddHours((timezoneDep.DayLightSavingOffset + timezoneDep.Offset) * -1));

```