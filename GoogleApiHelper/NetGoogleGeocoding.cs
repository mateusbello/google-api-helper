﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GoogleApiHelper
{
    /// <summary>
    ///   .Net utility for google geocoding API
    /// </summary>
    public static class NetGoogleGeocoding
    {

        /// <summary>
        /// The google geo code json service url.
        /// </summary>
        private static string GoogleGeoCodeJsonServiceUrl =
            "http://maps.googleapis.com/maps/api/geocode/json?address={0}&sensor=false";

        private static string GoogleGeoCodeLatLongJsonServiceUrl =
           "http://maps.googleapis.com/maps/api/geocode/json?latlng={0}&sensor=false";

        private static string GoogleGeoCodeTimeZoneUrl =
          "https://maps.googleapis.com/maps/api/timezone/json?location={0},{1}&timestamp={2}";


        /// <summary>
        /// The google geocode.
        /// </summary>
        /// <param name="address">
        /// The address.
        /// </param>
        /// <returns>
        /// The <see cref="GeocodeJsonResponse"/>.
        /// </returns>
        public static GeocodeJsonResponse GoogleGeocode(string address)
        {
            using (var cli = new WebClient())
            {
                var addressToValidate = string.Format(GoogleGeoCodeJsonServiceUrl, HttpUtility.UrlEncode(address));
                var response = cli.DownloadString(new Uri(addressToValidate));
                return HydrateJson(response);
            }
        }

        public static GeocodeJsonResponse GoogleGeocode(double latitude, double longitude)
        {
            using (var cli = new WebClient())
            {
                var addressToValidate = string.Format(GoogleGeoCodeLatLongJsonServiceUrl, latitude.ToString() + "," + longitude.ToString());
                var response = cli.DownloadString(new Uri(addressToValidate));
                return HydrateJson(response);
            }
        }

        public static TimeSpan GetFlightDuration(float departureLatitude, float departureLongitude,
            DateTime departureDateTime, float destinationLatitude, float destinationLongitude,
            DateTime destinationDateTime)
        {

            GoogleTimeZone timezoneDep = GetTimeZone(departureLatitude,departureLongitude,departureDateTime);
            GoogleTimeZone timezoneArr = GetTimeZone(destinationLatitude, destinationLongitude, destinationDateTime);

            var depTimeZone = TimeZoneInfo.CreateCustomTimeZone(timezoneDep.timeZoneId,
                new TimeSpan(timezoneDep.DayLightSavingOffset + timezoneDep.Offset, 0,0),"","");

            var arrTimeZone = TimeZoneInfo.CreateCustomTimeZone(timezoneArr.timeZoneId,
               new TimeSpan(timezoneArr.DayLightSavingOffset + timezoneArr.Offset, 0,0), "", "");



            DateTime depGMT = TimeZoneInfo.ConvertTimeToUtc(departureDateTime, depTimeZone);

            DateTime arrGMT = TimeZoneInfo.ConvertTimeToUtc(destinationDateTime, arrTimeZone);
            

            TimeSpan span = (arrGMT - depGMT);

            return span;


        }

        public static TimeSpan GetFlightDuration(string departureAddress, DateTime departureDateTime, string destinationAddress, DateTime destinationDateTime)
        {

            var geocodeDeparture = NetGoogleGeocoding.GoogleGeocode(departureAddress);
            var geocodeDestination = NetGoogleGeocoding.GoogleGeocode(destinationAddress);

           return GetFlightDuration(geocodeDeparture.GeoCodes[0].Latitude, geocodeDeparture.GeoCodes[0].Longitude,
                departureDateTime, geocodeDestination.GeoCodes[0].Latitude, geocodeDestination.GeoCodes[0].Longitude,
                destinationDateTime);
            
        }


        public static GoogleTimeZone GetTimeZone(float latitude, float longiture, DateTime timestamp)
        {
            using (var cli = new WebClient())
            {

                var addressToValidate = string.Format(GoogleGeoCodeTimeZoneUrl, latitude, longiture, UnixTimestampFromDateTime(timestamp));
                var response = cli.DownloadString(new Uri(addressToValidate));
                return HydrateTimezoneJson(response);
                
            }
        }


        public static GoogleTimeZone GetTimeZone(string address, DateTime timestamp)
        {
            string[] addressArr = address.Split(',');
            string addressTemp = string.Empty;
            if (addressArr.Length > 0)
            {
                addressTemp += addressArr[0];
            }
            if (addressArr.Length > 1)
            {
                addressTemp += ", " + addressArr[1];
            }

            var geocode = GoogleGeocode(addressTemp);

            using (var cli = new WebClient())
            {
                if (geocode != null && geocode.GeoCodes != null && geocode.GeoCodes.Count > 0)
                {
                    return GetTimeZone(geocode.GeoCodes[0].Latitude, geocode.GeoCodes[0].Longitude, timestamp);
                }
                return new GoogleTimeZone();
            }
        }

        private static long UnixTimestampFromDateTime(DateTime date)
        {
            long unixTimestamp = date.Ticks - new DateTime(1970, 1, 1).Ticks;
            unixTimestamp /= TimeSpan.TicksPerSecond;
            return unixTimestamp;
        }

        private static DateTime TimeFromUnixTimestamp(int unixTimestamp)
        {
            DateTime unixYear0 = new DateTime(1970, 1, 1);
            long unixTimeStampInTicks = unixTimestamp * TimeSpan.TicksPerSecond;
            DateTime dtUnix = new DateTime(unixYear0.Ticks + unixTimeStampInTicks);
            return dtUnix;
        }

        /// <summary>
        /// The get json node value.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetJsonNodeValue(JToken token, string field)
        {
            return token["address_components"].Children().Any(x => x["types"].Values<string>().Contains(field))
                       ? token["address_components"].Children().First(x => x["types"].Values<string>().Contains(field))[
                           "long_name"].Value<string>()
                       : string.Empty;
        }

        /// <summary>
        /// The hydrate json.
        /// </summary>
        /// <param name="jsonResponse">
        /// The json response.
        /// </param>
        /// <returns>
        /// The <see cref="GeocodeJsonResponse"/>.
        /// </returns>
        private static GeocodeJsonResponse HydrateJson(string jsonResponse)
        {
            GeocodeJsonResponse GoogleGeoCodeResponse = new GeocodeJsonResponse();
            GoogleGeoCodeResponse.GeoCodes = new List<Geocode>();

            var results = (JObject)JsonConvert.DeserializeObject(jsonResponse);
            foreach (var googleGeoCode in
                results["results"].Children()
                    .Select(
                        token =>
                        new Geocode
                        {
                            HouseNumber = GetJsonNodeValue(token, "street_number"),
                            StreetAddress = GetJsonNodeValue(token, "route"),
                            City = GetJsonNodeValue(token, "locality"),
                            County = GetJsonNodeValue(token, "administrative_area_level_2"),
                            State = GetJsonNodeValue(token, "administrative_area_level_1"),
                            Zip = GetJsonNodeValue(token, "postal_code"),
                            Country = GetJsonNodeValue(token, "country"),
                            FullAddress = token["formatted_address"].Value<string>(),
                            Types = token["types"].Values<string>().ToList(),
                            PointOfInterest = GetJsonNodeValue(token, "point_of_interest"),
                            LocationType = token["geometry"]["location_type"].Value<string>(),
                            Latitude = token["geometry"]["location"]["lat"].Value<float>(),
                            Longitude = token["geometry"]["location"]["lng"].Value<float>(),
                            Status = string.Format("{0}", token["geometry"]["location_type"].Value<string>()),
                            Premisse = GetJsonNodeValue(token, "premise"),
                            Establishment = GetJsonNodeValue(token, "establishment")
                        }))
            {
                GoogleGeoCodeResponse.GeoCodes.Add(googleGeoCode);
            }

            return GoogleGeoCodeResponse;
        }

        private static GoogleTimeZone HydrateTimezoneJson(string jsonResponse)
        {
            var result = (JObject)JsonConvert.DeserializeObject(jsonResponse);

            var timezone = new GoogleTimeZone();



            timezone.dstOffset = long.Parse((string)result["dstOffset"]);
            timezone.rawOffset = long.Parse((string)result["rawOffset"]);
            timezone.status = (string)result["status"];
            timezone.timeZoneId = (string)result["timeZoneId"];
            timezone.timeZoneName = (string)result["timeZoneName"];

            return timezone;


        }
    }
}
