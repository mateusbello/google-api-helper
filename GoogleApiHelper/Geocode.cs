﻿using System.Collections.Generic;

namespace GoogleApiHelper
{
    
        public class Geocode
        {

            public string HouseNumber { get; set; }
            public string StreetAddress { get; set; }
            public string City { get; set; }
            public string County { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string Country { get; set; }
            public string FullAddress { get; set; }
            public List<string> Types;
            public string LocationType { get; set; }
            public float Latitude { get; set; }
            public float Longitude { get; set; }
            public string Status { get; set; }
            public string PointOfInterest { get; set; }
            public string Premisse { get; set; }
            public string Establishment { get; set; }
            
        }
    
}
