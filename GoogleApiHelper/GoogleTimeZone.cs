﻿using System;

namespace GoogleApiHelper
{
    
        public class GoogleTimeZone
        {

            public long dstOffset { get; set; }
            public long rawOffset { get; set; }
            public string status { get; set; }
            public string timeZoneId { get; set; }
            public string timeZoneName { get; set; }

            public int Offset
            {
                get
                {
                    TimeSpan t = TimeSpan.FromSeconds( rawOffset );

                    return t.Hours;
                }
            }

            public int DayLightSavingOffset
            {
                get
                {
                    TimeSpan t = TimeSpan.FromSeconds(dstOffset);

                    return t.Hours;
                }
            }

        }
    
}
