﻿using System.Collections.Generic;

namespace GoogleApiHelper
{
    /// <summary>
    /// The geocode json response.
    /// </summary>
    public class GeocodeJsonResponse
    {
        /// <summary>
        /// Gets or sets the geo codes.
        /// </summary>
        public List<Geocode> GeoCodes { get; set; }
    }
}
