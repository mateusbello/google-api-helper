﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GoogleGeoCode.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <link href="Content/themes/base/jquery.ui.all.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <label>Location From</label>
        <asp:TextBox runat="server" CssClass="search" ID="locationFrom" Text="Belfast, UK"></asp:TextBox>
        <label>Location From Date</label>
        <asp:TextBox runat="server" ID="fromDate" Text="18/07/2014 13:17:07" placeholder="dd/MM/yyyy HH:mm:ss"></asp:TextBox>
        
        <br/>
         <br/>
        <label>Location To</label>
        <asp:TextBox runat="server" CssClass="search" Text="London, uk" ID="locationTo"></asp:TextBox>
        <label>Location To Date</label>
        <asp:TextBox runat="server" ID="toDate" Text="18/07/2014 13:17:00" placeholder="dd/MM/yyyy HH:mm:ss"></asp:TextBox>
        
         <br/>
         <br/>
       
        <asp:Button runat="server" ID="getTimeZone" Text="Get Time Zone" OnClick="getTimezoneDif"/>
        <asp:Button runat="server" ID="getmap" Text="Get map"/>
       
        <br/>
         <br/>
        <hr/>
        <div id="Information" style="float: left;padding-right: 100px;">
        <asp:Literal runat="server" ID="litResults"></asp:Literal>
          </div>
        <h2>Road Map Example</h2>
        <div id="map-canvas" style="height: 350px; width: 50%; margin: 0px; padding: 0px"></div>

    </div>
      
    </form>
    
    <script src="Scripts/jquery-2.1.1.min.js"></script>
    <script src="Scripts/jquery-ui-1.10.4.min.js"></script>
     <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
   
    <script>
        
        $(document).ready(function () {
            var map;
           
            var mapOptions = {
                zoom: 7,
                center: new google.maps.LatLng(parseFloat(<%= LatitudeDeparture %>), parseFloat(<%= LongitudeDeparture %>)),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
              };
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var directionsService = new google.maps.DirectionsService();


            $(".search").autocomplete({
                // The source option can be an array of terms.  In this case, if
                // the typed characters appear in any position in a term, then the
                // term is included in the autocomplete list.
                // The source option can also be a function that performs the search,
                // and calls a response function with the matched entries.
                source: "/AutoCompleteHandler.ashx",
                minLength: 1
            });
             
            var directionsRequest = {
                origin: new google.maps.LatLng(parseFloat(<%= LatitudeDeparture %>), parseFloat(<%= LongitudeDeparture %>)),
                destination: new google.maps.LatLng(parseFloat(<%= LatitudeDestination %>), parseFloat(<%= LongitudeDestination %>)),
                travelMode: google.maps.DirectionsTravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,

            }


            directionsService.route(directionsRequest, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {


                    new google.maps.DirectionsRenderer({
                        map: map,
                        directions: response,
                        suppressMarkers: false
                    });
                }
            });


           });

    </script>

</body>
</html>
