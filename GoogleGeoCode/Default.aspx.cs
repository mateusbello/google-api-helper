﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoogleApiHelper;

namespace GoogleGeoCode
{
    public partial class Default : System.Web.UI.Page
    {
        public float LatitudeDeparture { get; set; }
        public float LongitudeDeparture { get; set; }

        public float LatitudeDestination { get; set; }
        public float LongitudeDestination { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void getTimezoneDif(object sender, EventArgs e)
        {

            try
            {
                IFormatProvider culture = new CultureInfo("en-GB", true);

                // get timezone
                GoogleTimeZone timezoneDep = NetGoogleGeocoding.GetTimeZone(locationFrom.Text, DateTime.ParseExact(fromDate.Text, "dd/MM/yyyy HH:mm:ss", culture));
                GoogleTimeZone timezoneArr = NetGoogleGeocoding.GetTimeZone(locationTo.Text, DateTime.ParseExact(toDate.Text, "dd/MM/yyyy HH:mm:ss", culture));
                
                // get address info
                var geocode1 = NetGoogleGeocoding.GoogleGeocode(locationFrom.Text);

                LatitudeDeparture = geocode1.GeoCodes[0].Latitude;
                LongitudeDeparture = geocode1.GeoCodes[0].Longitude;
                
                var geocode2 = NetGoogleGeocoding.GoogleGeocode(locationTo.Text);

                LatitudeDestination = geocode2.GeoCodes[0].Latitude;
                LongitudeDestination = geocode2.GeoCodes[0].Longitude;

                // calculate the time diference
                var span = NetGoogleGeocoding.GetFlightDuration(LatitudeDeparture, LongitudeDeparture,
                    DateTime.ParseExact(fromDate.Text, "dd/MM/yyyy HH:mm:ss", culture), LatitudeDestination, LongitudeDestination,
                    DateTime.ParseExact(toDate.Text, "dd/MM/yyyy HH:mm:ss", culture));


                litResults.Text = "<h1>" + locationFrom.Text + "</h1>";
                litResults.Text += "<h2>Lat: " + geocode1.GeoCodes[0].Latitude + " Long: " + geocode1.GeoCodes[0].Longitude + "</h2>";

                litResults.Text += "TimeZoneName: " + timezoneDep.timeZoneName;
                litResults.Text += "<br/>DayLightSavingOffset: " + timezoneDep.DayLightSavingOffset;
                litResults.Text += "<br/>Offset: " + timezoneDep.Offset;
                litResults.Text += "<br/>dstOffset: " + timezoneDep.dstOffset;
                litResults.Text += "<br/>rawOffset: " + timezoneDep.rawOffset;
                litResults.Text += "<br/>status: " + timezoneDep.status;

                litResults.Text += "<br/>";
                litResults.Text += "<h1>" + locationTo.Text + "</h1>";
                litResults.Text += "<h2>Lat: " + geocode2.GeoCodes[0].Latitude + " Long: " + geocode2.GeoCodes[0].Longitude + "</h2>";

                litResults.Text += "TimeZoneName: " + timezoneArr.timeZoneName;
                litResults.Text += "<br/>DayLightSavingOffset: " + timezoneArr.DayLightSavingOffset;
                litResults.Text += "<br/>Offset: " + timezoneArr.Offset;
                litResults.Text += "<br/>dstOffset: " + timezoneArr.dstOffset;
                litResults.Text += "<br/>rawOffset: " + timezoneArr.rawOffset;
                litResults.Text += "<br/>status: " + timezoneArr.status;

                litResults.Text += "<br/>";
                litResults.Text += "<h1> Time Difs </h1>";
                litResults.Text += "<br/>Days: " + span.Days;
                litResults.Text += "<br/>Hours: " + span.Hours;
                litResults.Text += "<br/>Minutes: " + span.Minutes;
            }
            catch (Exception ex)
            {

                litResults.Text = ex.Message;
            }

           
            
        }
    }
}