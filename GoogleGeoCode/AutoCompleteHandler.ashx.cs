﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using GoogleApiHelper;

namespace GoogleGeoCode
{
    /// <summary>
    /// Summary description for AutoCompleteHandler
    /// </summary>
    public class AutoCompleteHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/json";

            string term = HttpContext.Current.Request.QueryString["term"];
            var results = new List<string>();

            ;
            var response = NetGoogleGeocoding.GoogleGeocode(term);
            foreach (var location in response.GeoCodes)
            {
                results.Add(location.FullAddress);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            string res = js.Serialize(results.Select(x => x));
            context.Response.Write(res);
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}